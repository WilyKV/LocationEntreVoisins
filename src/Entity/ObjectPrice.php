<?php

namespace App\Entity;

use App\Repository\ObjectPriceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ObjectPriceRepository::class)]
class ObjectPrice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\Column(length: 5)]
    private ?string $currency = null;

    #[ORM\Column(nullable: true)]
    private ?float $numberDays = null;

    #[ORM\Column(nullable: true)]
    private ?float $numberHours = null;

    #[ORM\Column(nullable: true)]
    private ?int $numberMinutes = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getNumberDays(): ?float
    {
        return $this->numberDays;
    }

    public function setNumberDays(?float $numberDays): self
    {
        $this->numberDays = $numberDays;

        return $this;
    }

    public function getNumberHours(): ?float
    {
        return $this->numberHours;
    }

    public function setNumberHours(?float $numberHours): self
    {
        $this->numberHours = $numberHours;

        return $this;
    }

    public function getNumberMinutes(): ?int
    {
        return $this->numberMinutes;
    }

    public function setNumberMinutes(?int $numberMinutes): self
    {
        $this->numberMinutes = $numberMinutes;

        return $this;
    }
}
