<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminisitrationController extends AbstractController
{
    #[Route('/adminisitration', name: 'app_adminisitration')]
    public function index(): Response
    {
        return $this->render('adminisitration/index.html.twig', [
            'controller_name' => 'AdminisitrationController',
        ]);
    }
}
