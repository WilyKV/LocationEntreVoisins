<?php

namespace App\Entity;

use App\Repository\AdLocalisationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdLocalisationRepository::class)]
class AdLocalisation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $country = null;

    #[ORM\Column(length: 100)]
    private ?string $department = null;

    #[ORM\Column(length: 75)]
    private ?string $city = null;

    #[ORM\Column]
    private ?int $postalCode = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $street = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $additional = null;

    #[ORM\Column]
    private ?bool $displaySpecificAddress = null;

    #[ORM\Column]
    private ?bool $displayGlobalAddress = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDepartment(): ?string
    {
        return $this->department;
    }

    public function setDepartment(string $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }

    public function setAdditional(?string $additional): self
    {
        $this->additional = $additional;

        return $this;
    }

    public function isDisplaySpecificAddress(): ?bool
    {
        return $this->displaySpecificAddress;
    }

    public function setDisplaySpecificAddress(bool $displaySpecificAddress): self
    {
        $this->displaySpecificAddress = $displaySpecificAddress;

        return $this;
    }

    public function isDisplayGlobalAddress(): ?bool
    {
        return $this->displayGlobalAddress;
    }

    public function setDisplayGlobalAddress(bool $displayGlobalAddress): self
    {
        $this->displayGlobalAddress = $displayGlobalAddress;

        return $this;
    }
}
