<?php

namespace App\Entity;

use App\Repository\ObjectCharacteristicRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ObjectCharacteristicRepository::class)]
class ObjectCharacteristic
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $specification = null;

    #[ORM\Column(nullable: true)]
    private ?int $priorityRanking = null;

    #[ORM\Column]
    private ?bool $display = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSpecification(): ?string
    {
        return $this->specification;
    }

    /**
     * @param string $specification
     * @return $this
     */
    public function setSpecification(string $specification): self
    {
        $this->specification = $specification;

        return $this;
    }

    public function getPriorityRanking(): ?int
    {
        return $this->priorityRanking;
    }

    public function setPriorityRanking(?int $priorityRanking): self
    {
        $this->priorityRanking = $priorityRanking;

        return $this;
    }

    public function isDisplay(): ?bool
    {
        return $this->display;
    }

    public function setDisplay(bool $display): self
    {
        $this->display = $display;

        return $this;
    }
}
